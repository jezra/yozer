/*
--- TODO ---
* context
* reply all
*
* display acurate 'from': find the original client
* display attachments
*/

//global variables
apiroot = undefined;
most_recent_status_id = 0;
most_recent_stream_id = 0;
most_recent_mention_id = 0;
stream_timeout = 0;
mentions_timeout = 0;
status_timeout = 0;
names_from_url = {};
//TODO: read the prune lengths from the settings page
dents_prune_length = 100;
mentions_prune_length = 100;
selected_tab_content = undefined;
current_display_page = undefined;
//who the fuck am I?
client_name = "yozer"

// the function to run first
$(function(){
  // test if localstorage is available
  try {
    localStorage.setItem('1','2');
    localStorage.removeItem('1');
    has_local_storage = true;
  } catch(e) {
    alert(e);
    has_local_storage = false;
  }
  if (has_local_storage) {
    setup_external_toolbars();
    set_up_buttons();
    get_stored_data();
    set_up_settings();
    set_up_new_status();
  } else {
    //let the user know about the issue
    $("#content").html("Sorry Buddy,<br /> Your browser does not support HTML5 localStorage.");
  }
});


function setup_external_toolbars(){
  $("[data-role='navbar']").navbar({ theme: "b"});
  $("[data-role='header'], [data-role='footer']" ).toolbar({ theme: "b"});
  $("[data-role='panel']").panel({ theme: "b"}).enhanceWithin({ theme: "b"});
}

/* set up the buttons */
function set_up_buttons(){
  //Dents
  $("#button_status").click(function(){
    $("#button_status").addClass('yzr-tab-selected');
    $("#button_mentions").removeClass('yzr-tab-selected');
    $("#button_stream").removeClass('yzr-tab-selected');
  });
  
  //Mentions
  $("#button_mentions").click(function(){
    //update tab graphics
    $("#button_status").removeClass('yzr-tab-selected');
     $("#button_stream").removeClass('yzr-tab-selected');
    $("#button_mentions").addClass('yzr-tab-selected');
  });  
  
  //stream
  $("#button_stream").click(function(){
    //update tab graphics
    $("#button_status").removeClass('yzr-tab-selected');
     $("#button_stream").addClass('yzr-tab-selected');
    $("#button_mentions").removeClass('yzr-tab-selected');
  });  
  
  //Config
  $("#button_config").click(function(){
    //show page config
  });
}

/* local stored data */
function get_stored_data() {
  // get various data items and store them in global variables?
  username = localStorage["username"];
  password = localStorage["password"];
  //TODO: make sure the password is readable by yozer only
  service = localStorage["service"];

  if(username && password  && service ) {
    authenticate(username, password, service);
  } else {
    if (!service) {
    //if service is undefined, try using the hostname
    var pu = parse_url(document.location.href);
      service = pu.protocol+"//"+pu.hostname;
    }
    //click the config button
    $('#button_settings').click();
    
  }
}

/* settings */
function set_up_settings() {
  //set info into the settings
  $("#authenticate input[name='service']").val(service);
  $("#authenticate input[name='username']").val(username);
  $("#authenticate input[name='password']").val(password);
  //set an action for the 'submit' button
  $("#authenticate button[name='submit']").on('click', click_authenticate_submit);

  //did a form submit happen?
  $("#authenticate_form").submit(function(){
    console.log('formsubmit');
  });
  $("#clear_localstorage").on('click', click_clear_localstorage);
}

/* new status */
function set_up_new_status() {
  //hide the 'to' stuff
  $("#new_status_reply_to").hide();
  //add a click listener to the submit button
  $("#new_status button[name='submit']").on('click', click_new_status_submit);
  $("#new_status button[name='clear']").on('click', click_new_status_clear);
}

function set_apiroot(service) {
  //does the service start with http?
  if( service.indexOf("https://")!=0 ) {
    if( service.indexOf("http://")!=0 ) {
      service = "http://"+service;
    }
  }

  //set the global apiroot
  apiroot = service+"/api";
}

function set_authhash(username,password) {
  auth_hash =  btoa(username + ":" + password);
}

// authenticate rquest
function click_authenticate_submit() {
  var button = $("#authenticate button[name='submit']");
  //button.prop( "disabled", true );
  var service = $("#authenticate input[name='service']").val().trim();
  var username = $("#authenticate input[name='username']").val().trim();
  var password = $("#authenticate input[name='password']").val().trim();
  // do we have service, username and password?
  if (service!='' || username != '' || password!='') {
    //try to authenticate against the service
    authenticate(username, password, service);
  }
}

function authenticate(username, password, service) {
  set_apiroot(service);
  set_authhash(username, password);
  $.ajax ({
    type: 'GET',
    beforeSend: set_auth_headers,
    url: apiroot+"/account/verify_credentials.xml",
    dataType: 'xml',
    crossDomain: true,
    success: function (){
      user_authenticated(username, password, service);
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
      alert('Authentication Error');
      //show the settings page
    }
  });
}


function user_authenticated(username, password, service) {
  //authentication was successful
  authenticated = true;
  //record the data
  localStorage["username"] = username;
  localStorage["password"] = password;
  localStorage["service"] = service;

  //get the dents!
  request_stream();
  request_statuses();
  request_mentions();
  //click the dents tab
  $("#button_stream").click();
  //close the settings panel
  $("#panel_settings").panel('close');
  
}

//user clicked new status submit
function click_new_status_submit() {
  //disable the input
  var text = $("#new_status textarea[name='status']").val();
  var text = text.trim();
  if (text != "") {
    //disable most of the form
    $("#new_status textarea[name='status']").prop('disabled',true);
    $("#new_status button[name='submit']").prop('disabled',true);
    request_post_new_status();
  }
}

//user clicked new dent clear
function click_new_status_clear() {
  clear_new_status_form();
}

function clear_new_status_form() {
  //clear the data
  $("#new_status textarea[name='status']").val('');
  // add the OG status's author nickname to the new status form
  $("#new_status_reply_to_name").text('');
  //show the new status reply to
  $("#new_status_reply_to").hide();
  //empty the 'in reply to'
  $("#in_reply_to").empty();
  //clear the field value
  $("#new_status input[type='file']").val('');
  //enable the text field
  $("#new_status textarea[name='status']").prop("disabled", false);

}

function click_context_button(event) {
  //context page is not closed
  context_closed = false;

  var button = $(event['currentTarget']);
  var dent = $(button).closest(".dent");
  //get the local_id of the dent
  var url = dent.attr('url');
  current_context_root = url;
  console.log(current_context_root);
  //record the current tab
  pre_context_page = current_display_page;
  //clear the context tab contents
  $("#context_list").empty();
  //display the context page
  display_page("context");
  //go get that shit!
  request_context(url, url);
}


function request_context(context_root, url) {
  //is the context_root the current context root?
  if(context_root != current_context_root) {
    return;
  }
  //transform the URL to get the appropriate .atom URL of the status
  var new_url = url.replace("/notice/", "/api/statuses/show/");
  new_url+=".atom";
  $.ajax ({
    type: 'GET',
    //beforeSend: set_auth_headers,
    url: new_url,
    dataType: 'xml',
    crossDomain: true,
    success: function (data){
      //convert this shitty xml to usable json
      data =  $.xml2json(data);;
      if (context_root == current_context_root && !context_closed) {
        //append the stuff
        var dent = status_to_html_thingy(data, true);
        $("#context_list").append(dent);
        //is this data thang in reply to something?
        if (data['object']['inReplyTo']) {
          //keep getting more context
          request_context(context_root, data['object']['inReplyTo']);
        }
      }
      //
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
    }
  });


}

function click_reply_button(event) {
  var button = $(event['currentTarget']);
  //get the parent status of the button
  var status = $(button).closest(".status");
  //get the local_id of the status
  var local_status_id = status.attr('status_id');
  //get the creators name
  var reply_to_username = status.children(".authorname").text();
  // add the OG status's author nickname to the new status form
  $("#new_status_reply_to_name").text(reply_to_username);
  //show the new status reply to
  $("#new_status_reply_to").show();
  // create a hidden 'in reply to' field to the new status form
  var in_reply_to_field = $("<input type='hidden' name='in_reply_to_status_id' value='"+local_status_id+"'>");
  //clear the 'in_reply_to'
  $("#in_reply_to").empty();
  //add the hidden field
  $("#in_reply_to").append(in_reply_to_field);
  // 'click' the new status tab
  $("#button_new_status").click();
}

function click_reply_all_button(event) {
  var button = $(event['currentTarget']);
  //get the parent dent of the button
  var dent = $(button).closest(".dent");
  //get the local_id of the dent
  var local_status_id = dent.attr('status_id');
  //get the creators name
  var reply_to_username = dent.children(".authorname").text();
  //add the username to the default dent text

  //get the spans with class=to_name where content doesn't start with !
  var to_names = dent.find(".to_name");
  console.log(to_names);
  var to_addendum  = "";
  $.each( to_names, function(index, to_name){
    var name = $(to_name).HTML();
    if (name.indexOf("!") != 1) {
      to_addendum+="@"+name+" ";
    }
  });
  alert( to_addendum );
  $("#new_dent textarea[name='status']").val(to_addendum);
  // add the OG dent's author nickname to the new dent form
  $("#new_dent_reply_to_name").text(reply_to_username);
  //show the new dent reply to
  $("#new_dent_reply_to").show();
  // create a hidden 'in reply to' field to the new dent form
  var in_reply_to_field = $("<input type='hidden' name='in_reply_to_status_id' value='"+local_status_id+"'>");
  //clear the 'in_reply_to'
  $("#in_reply_to").empty();
  //add the hidden field
  $("#in_reply_to").append(in_reply_to_field);
  // 'click' the new dent tab
  $("#new_dent").click();
}

function request_post_new_status() {
  //gather the data
  var fd = new FormData();
  fd.append('status', $("#new_status textarea[name='status']").val() );
  fd.append('source', client_name);
  if( $("#new_status input[name='in_reply_to_status_id']")[0] ) {
    fd.append('in_reply_to_status_id', $("#new_status input[name='in_reply_to_status_id']").val() );
  }
  //why can I not get this info with a jquery selector?
  var f = document.getElementById("file_upload").files[0];
  if (f) {
    fd.append('media', f);
  }

  $.ajax ({
    type: 'POST',
    beforeSend: set_auth_headers,
    url: apiroot+"/statuses/update.xml",
    data: fd,
    dataType: 'xml',
    processData: false,
    contentType: false,
    crossDomain: true,
    success: function (){
      /* dent was posted! */
      //clear the form
      clear_new_status_form();
      //close the new status panel
      $("#panel_new_status").panel("close");
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
      alert('error posting dent');
    }
  });
 //enable form elements
  $("#new_status textarea[name='status']").prop('disabled',false);
  $("#new_status button[name='submit']").prop('disabled',false);
}

function click_clear_localstorage() {
// clear the localstorage
localStorage["username"] = '';
localStorage["password"] = '';
localStorage["service"] = '';
alert('localStorage has been cleared');

}

function start_status_timeout() {
  status_timeout = setTimeout( request_statuses, 60000 );
}
function start_stream_timeout() {
  stream_timeout = setTimeout( request_stream, 60000 );
}

function request_name_for_url(url) {
  /* parse info from the url */
  //is this a user or group?
  if ( url.indexOf('/user/')!=-1 ){
    var new_url = url.replace('user/', "api/users/show.json?id=");
    // is this really a group?
  } else if (url.indexOf('/group/')!=-1) {
    var new_url = url.replace('group/', "api/statusnet/groups/show/");
    new_url = new_url.replace('/id', '.json');
  } else {
    console.log("Unknown 'to' type for: "+url);
    return;
  }
  //try to get the name associated with the user at this url
  $.ajax ({
    type: 'GET',
    url: new_url,
    dataType: 'json',
    crossDomain: true,
    success: function (data){
      process_name_for_url(data, url);
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
    }
  });
}

function request_stream() {
  //what URL do we need to parse?
  var url = apiroot+"/statuses/friends_timeline/"+localStorage['username']+".atom?since="+most_recent_stream_id;
  //try to get some statuses for this user
  $.ajax ({
    type: 'GET',
    beforeSend: set_auth_headers,
    url: url,
    dataType: 'xml',
    crossDomain: true,
    success: function (data){
      process_status_items(data,'stream');
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
    }
  });
  // go get some more statuss in a bit 
  start_stream_timeout();
}

function request_statuses() {
  //what URL do we need to parse?
  var url = apiroot+"/statuses/user_timeline/"+localStorage['username']+".atom?since="+most_recent_status_id;
  //try to get some statuses for this user
  $.ajax ({
    type: 'GET',
    beforeSend: set_auth_headers,
    url: url,
    dataType: 'xml',
    crossDomain: true,
    success: function (data){
      process_status_items(data,'status');
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
    }
  });
  // go get some more statuss in a bit 
  start_status_timeout();
}

function start_mentions_timeout() {
  mentions_timeout = setTimeout( request_mentions, 90000 );
}

function request_mentions() {
  //what URL do we need to parse?
  var url = apiroot+"/statuses/mentions/"+localStorage['username']+".atom?since="+most_recent_mention_id;
  //try to get mentions for this user
  $.ajax ({
    type: 'GET',
    beforeSend: set_auth_headers,
    url: url,
    dataType: 'xml',
    crossDomain: true,
    success: function (data){
      process_status_items(data,'mentions');
    },
    error: function(xhr, status, error){
      ajax_failure(xhr, status, error);
    }
  });
  //check for more 
  start_mentions_timeout();
}

function set_auth_headers(xhr) {
  xhr.setRequestHeader("Authorization", "Basic "+auth_hash);
}


/*    DATA processing     */
function process_status_items(data, type ) {
  //convert this shitty xml to usable json
  data = $.xml2json(data);
  //reverse the data array
  var items = data.entry.reverse();
  $.each(items, function(i, dent){
    var id = dent['notice_info']['local_id'];
    if (type == 'status' ) {
      if (id > most_recent_status_id) {
        var d = status_to_html_thingy(dent, false);
        $("#status_list").prepend(d);
        d.trigger('create');
        most_recent_status_id = id;
      }
    } else if(type == 'mentions') {
      if (id > most_recent_mention_id) {
        var d = status_to_html_thingy(dent, false);
        $("#mentions_list").prepend(d);
        //trigger jquery mobile
        d.trigger('create');
        most_recent_mention_id = id;
      }
    } else if(type == 'stream') {
      if (id > most_recent_stream_id) {
        var d = status_to_html_thingy(dent, false);
        $("#stream_list").prepend(d);
        //trigger jquery mobile
        d.trigger('create');
        most_recent_stream_id = id;
      }
    }
  });
}

function get_name_for_url(url) {
  var name = names_from_url[ url ];
  //does this url exist in the names_from_url list?
  if (name == undefined){
    //set the name to empty string
    name = "<a href='"+url+"'>?</a>";
    names_from_url[url] = name;
    //request the name
    request_name_for_url( url );
  }
  return name;
}

function process_name_for_url(data, url) {
  if (url.indexOf("/user/")!=-1 ) {
    var name = data['screen_name'];
  } else {
    var name = "!"+data.nickname;
  }
  //add the name to the array

  names_from_url[url] = name;
  //add this name to all the .to_name with url = to_url
  $(".actor_name[actor_name_url='"+url+"']").text(name);
}

function status_to_html_thingy(status, ignore_context) {
  //is there a better/tempately way to do this? this just seems so shitty

  // get the author
  var author = status['author']; //because the fucking API is inconsistent
  var author_name = "?";
  var author_url = "?";
  
  if (author != undefined ) {
    var author_name = author.name;
    var author_url = author.uri;
    //use a thumbname avatar_element
    /* get a URL of the avatar */
    var avatar_src = author['link'][3]["href"];
  } else {
    //meh?
  }

  var id = status['notice_info'].local_id;
  
  var url = status['link'].href;
  //put this name into the names_from_url collection
  names_from_url[author_url]=author_name;

  //create some elements
  var status_element = $("<div class='status' url='"+url+"' status_id='"+id+"'>");
  var avatar_element = $("<img class='avatar'>");
  var authorname_element = $("<span class='authorname actor_name' actor_name_url='"+author_url+"'>");
  var published_element = $("<div class='published'>");
  var buttons_element = $("<div class='buttons' data-role='controlgroup' data-type='horizontal' data-mini='true'>");
  //get data from the status
  var publish_date = status['published'];
  var text = status['content'];
  //TODO: sanitize the text!!!!!!
  
  //get images from the text
  var linked_images = [];
  var holder = $("<div>");
  holder.append(text.toString());
  holder.children("a").each(function(i,e) {
    var href = $(e).prop('href');
    var title = $(e).prop('title');

    if (is_image_url(href) ) {
      if ($.inArray(href, linked_images) == -1 ){
        linked_images.push(href);
      }
    }
    if (is_image_url(title) ) {
      if ($.inArray(title, linked_images) == -1 ){
        linked_images.push(title);
      }
    }
  });
  
  avatar_element.attr('src', avatar_src);
  authorname_element.text(author_name);
  published_element.text( publish_date);

  var mentioned_thing_urls = [];

  var to_user_count = 0;
  var to_users = '';
  //loop through the links to see if anyone is mentioned
  $.each( status['link'], function(index, link){
    //is the ref 'mentioned'?
    if (link.rel == 'mentioned') {
      //is this a user or group?
      if (link.href.indexOf("/user/")!=-1 || link.href.indexOf("/group/")!=-1) {
        //this is a mention
        mentioned_thing_urls.push(link.href);
        //is this a user mention?
        if (link.href.indexOf("/user/")!=-1) {
          to_user_count += 1;
        }
      }
    }
  });
  var to_wrapper_element = undefined;
  //are there any mentioned things?
  if (mentioned_thing_urls.length > 0 ) {
    //create some elements
    to_wrapper_element = $("<span class='status_to'>");
    var to_icon = $("<span class='to_icon'>➣</span>");
    to_wrapper_element.append(to_icon);

    //loop through the to
    for(var i=0; i < mentioned_thing_urls.length; i++) {
      var to_url = mentioned_thing_urls[i];
      var to_element = $("<span class='to_name actor_name' actor_name_url='"+to_url+"'>");
      //try to get the to name from the url
      var to_name = get_name_for_url(to_url );
      to_element.html(to_name);
      to_wrapper_element.append( to_element );
    }
  }

  //make some button to do shit
  var reply_button = $("<button data-role='button' class='ui-btn ui-btn-inline'>Reply</button>");
  reply_button.on('click', click_reply_button);
  buttons_element.append(reply_button);

  //if there are multiple 'to users' create a reply all button
  if (to_user_count > 1) {
    var reply_all_button = $("<button data-role='button' class='ui-btn ui-btn-inline'>Reply All</button>");
    reply_all_button.on('click', click_reply_all_button);
    buttons_element.append(reply_all_button);
  }

  // is this in reply to something?
  /*
  if (object['inReplyTo'] && !ignore_context) {
    //create a 'context' button
    var context_button = $("<button class='context' context_url='"+object['url']+"'>");
    context_button.text('Context');
    context_button.on('click', click_context_button);
    buttons_element.append(context_button);
  }*/

  //put it all together
  status_element.append(avatar_element);
  status_element.append(authorname_element);
  if(to_wrapper_element != undefined) {
    status_element.append(to_wrapper_element);
  }
  status_element.append(published_element);

  status_element.append( text );
  //handle images
  if (linked_images.length > 0) {
    var status_images =  $("<div class='status_images'>");
    status_element.append( status_images);
    $.each(linked_images, function(index,image_path) {
      var img = $("<img src='"+image_path+"'>");
      status_images.append(img);
    }); 
  }
  //TODO: sanitize the statusnet_html
  status_element.append( $("<div class='clearfix'>") );
  //add the buttons
  status_element.append( buttons_element );
  return status_element;
}
