function parse_url(url) {
	 var link = document.createElement("a");
		link.href = url;
		return link;
		//accessible properties of 'link'
		link.protocol; //	"http:"
		link.host;		 //	"www.jezra.net:12345"
		link.hostname; //	"www.jezra.net"
		link.port;		 //	"12345"
		link.pathname; //	"/path/name/"
		link.hash;		 //	"#hash"
		link.foo;			//	"?foo=bar"
}

function ajax_failure(xhr, status, error) {
	console.log("Status: "+status+"\nError: "+error);
	console.log(xhr.getAllResponseHeaders() );
	console.log(xhr.responseText);
	console.log(xhr.responseXML);
}


function endsWith(str, suffix) {
  return str.indexOf(suffix, str.length - suffix.length) !== -1;
}


function is_image_url(url) {
  var l = url.toLowerCase();

  if ( endsWith(l, 'jpeg') || endsWith(l, 'jpg') || endsWith(l, 'png') || endsWith(l, 'gif') ) {
    console.log(l);
    return true;
  } else {
    return false;
  }
}
